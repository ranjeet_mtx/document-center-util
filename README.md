# DESCRIPTION #
A generic Util Lightning Web Component to maintain all Document Links in one page. Admin can manage Document/Video links by  just creating/deleting records under “Document Center” custom metadata type. Admin can also rearrange the Documents by adjusting “display order field”. 

# HOW TO USE IN AURA #
Create a Aura Component and add following code :
<c:document_center></c:document_center>