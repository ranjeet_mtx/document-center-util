/**
 * @description Controller for document_center Lightning Web Component
 * @test DocumentCenterControllerTest
 * @author Ranjeet
 */
public with sharing class DocumentCenterController {
    
    /**
     * @description QUERY_ALL document center records.
     */
    @AuraEnabled
    public static List<Document_Center_Link__mdt> getDocumentCenterLinks() {
        List<Document_Center_Link__mdt> allLinks = [
            Select Id, DeveloperName, Display_Order__c, 
            Document_Button_Label__c, Document_Description__c, 
            Document_Heading__c, Document_Link__c 
            From Document_Center_Link__mdt 
            Order By Display_Order__c ASC
        ];
        return allLinks;
    }
}