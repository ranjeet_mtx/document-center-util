/**
 * @description Document Center Component JS Controller
 * @author Ranjeet
 */

import { LightningElement, api, track } from 'lwc';

// Importing Apex
import getDocumentCenterLinks from '@salesforce/apex/DocumentCenterController.getDocumentCenterLinks';

export default class Document_Center extends LightningElement {

    @track documentLinks;

    connectedCallback() {
        this.documentLinks = [];
        getDocumentCenterLinks()
        .then( allLinks => {
            this.documentLinks = allLinks;
        })
        .catch( error => {
            console.log("ERROR --> " + JSON.stringify(error));
        })
    }
    
    redirectToDocumentLink(event) {
        let docLink = event.target.dataset.link;
        if(docLink) {
            window.open(docLink, "_blank");
        }
    }
}